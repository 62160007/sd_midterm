/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.softdev_midterm;

import java.io.Serializable;

/**
 *
 * @author tatar
 */
public class Item implements Serializable {

    private int id;
    private String name;
    private String brane;
    private double price;
    private int amount;

    Item(int id, String name, String brane, double price, int amount) {
        this.id = id;
        this.name = name;
        this.brane = brane;
        this.price = price;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrane() {
        return brane;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrane(String brane) {
        this.brane = brane;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Id = " + id + " Name = " + name + " Brand = " + brane
                + " Price = " + price + " Amount = " + amount;
    }
}
