/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.softdev_midterm;

import java.awt.HeadlessException;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author tatar
 */
public class MenuPanel extends javax.swing.JPanel {

    /**
     * Creates new form MenuPanel
     */
    private DefaultListModel model;
    int modeSelect = 0;
    double sumPrice = 0;
    int sumAmount = 0;

    public MenuPanel() {
        initComponents();
        model = new DefaultListModel();
        lstItem.setModel(model);

        refresh();
        hideConsole();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        txtID = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        txtBrand = new javax.swing.JLabel();
        txtPrice = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        edtID = new javax.swing.JTextField();
        edtBrand = new javax.swing.JTextField();
        edtName = new javax.swing.JTextField();
        edtAmount = new javax.swing.JTextField();
        edtPrice = new javax.swing.JTextField();
        txtMode = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        txtTotalPrice = new javax.swing.JLabel();
        txtTotalItem = new javax.swing.JLabel();
        txtSumPrice = new javax.swing.JLabel();
        txtSumItem = new javax.swing.JLabel();
        txtBaht = new javax.swing.JLabel();
        txtItem = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstItem = new javax.swing.JList<>();

        txtID.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtID.setText("ID:");

        txtName.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtName.setText("Name:");

        txtBrand.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtBrand.setText("Brand:");

        txtPrice.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtPrice.setText("Price:");

        txtAmount.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtAmount.setText("Amount:");

        edtID.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        edtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtIDActionPerformed(evt);
            }
        });

        edtBrand.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        edtBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtBrandActionPerformed(evt);
            }
        });

        edtName.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        edtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtNameActionPerformed(evt);
            }
        });

        edtAmount.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        edtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtAmountActionPerformed(evt);
            }
        });

        edtPrice.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        edtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtPriceActionPerformed(evt);
            }
        });

        txtMode.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtMode.setText("...");

        btnSave.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtID, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBrand)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtBrand, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPrice)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAmount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancel))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtMode, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(txtMode, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID)
                    .addComponent(txtName)
                    .addComponent(txtBrand)
                    .addComponent(txtPrice)
                    .addComponent(txtAmount)
                    .addComponent(edtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtBrand, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnAdd.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnClear.setText("Clear All");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        txtTotalPrice.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtTotalPrice.setText("Total Price : ");

        txtTotalItem.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtTotalItem.setText("Total : ");

        txtSumPrice.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtSumPrice.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtSumPrice.setText("00.00");

        txtSumItem.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtSumItem.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtSumItem.setText("0");

        txtBaht.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtBaht.setText("Baht");

        txtItem.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        txtItem.setText("Item");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtTotalPrice)
                    .addComponent(txtTotalItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSumPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSumItem, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBaht)
                    .addComponent(txtItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addGap(18, 18, 18)
                        .addComponent(btnEdit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addGap(18, 18, 18))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnEdit)
                            .addComponent(btnDelete)
                            .addComponent(btnAdd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClear))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotalPrice)
                            .addComponent(txtSumPrice)
                            .addComponent(txtBaht))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotalItem, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSumItem)
                            .addComponent(txtItem))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lstItem.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        jScrollPane1.setViewportView(lstItem);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    private void visibleControl(boolean show) {
        if (modeSelect < 0) {
            txtMode.setText("Add item");
        } else if (modeSelect > 0) {
            txtMode.setText("Edit item");
        }
        visibleTxt(show);
        visibleEdt(show);
        btnSave.setVisible(show);
        btnCancel.setVisible(show);
        jPanel2.setVisible(show);
    }

    private void visibleEdt(boolean show) {
        edtID.setVisible(show);
        edtName.setVisible(show);
        edtBrand.setVisible(show);
        edtPrice.setVisible(show);
        edtAmount.setVisible(show);
    }

    private void visibleTxt(boolean show) {
        txtID.setVisible(show);
        txtName.setVisible(show);
        txtBrand.setVisible(show);
        txtPrice.setVisible(show);
        txtAmount.setVisible(show);
        txtMode.setVisible(show);
    }

    private void showConsole() {
        visibleControl(true);
    }

    private void hideConsole() {
        visibleControl(false);
    }

    private void clearData() {
        edtID.setText("");
        edtName.setText("");
        edtBrand.setText("");
        edtPrice.setText("");
        edtAmount.setText("");
        hideConsole();
    }

    private void saveAdd() {
        int id = Integer.parseInt(edtID.getText());
        String name = edtName.getText();
        String brand = edtBrand.getText();
        double price = Double.parseDouble(edtPrice.getText());
        int amount = Integer.parseInt(edtAmount.getText());
        Item item = new Item(id, name, brand, price, amount);
        MenuService.addItem(item);
    }

    private void saveEdit() {
        int id = Integer.parseInt(edtID.getText());
        String name = edtName.getText();
        String brand = edtBrand.getText();
        double price = Double.parseDouble(edtPrice.getText());
        int amount = Integer.parseInt(edtAmount.getText());
        Item item = new Item(id, name, brand, price, amount);
        MenuService.updateItem(modeSelect, item);
    }

    public void refresh() {
        model.clear();
        model.addAll(MenuService.getItem());
        checkTotal();
        txtSumPrice.setText(String.valueOf(sumPrice));
        txtSumItem.setText(String.valueOf(sumAmount));
    }

    public void checkTotal() {
        sumPrice = 0;
        sumAmount = 0;
        for (int i = 0; i < MenuService.getSize(); i++) {
            sumPrice += MenuService.getPrice(i);
            sumAmount += MenuService.getAmount(i);
        }
    }
    private void edtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtIDActionPerformed

    private void edtBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtBrandActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtBrandActionPerformed

    private void edtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtNameActionPerformed

    private void edtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtAmountActionPerformed

    private void edtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtPriceActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        if (modeSelect < 0) {
            saveAdd();
        } else if (modeSelect >= 0) {
            saveEdit();
        }
        refresh();
        clearData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        modeSelect = -1;
        showConsole();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        modeSelect = lstItem.getSelectedIndex();
        if (modeSelect < 0) {
            return;
        }
        Item item = MenuService.getItem(modeSelect);
        edtID.setText(String.valueOf(item.getId()));
        edtName.setText(item.getName());
        edtBrand.setText(item.getBrane());
        edtPrice.setText(String.valueOf(item.getPrice()));
        edtAmount.setText(String.valueOf(item.getAmount()));
        showConsole();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        clearData();
        refresh();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        modeSelect = lstItem.getSelectedIndex();
        if (modeSelect < 0) {
            return;
        }
        String confirmItem = MenuService.getItem(modeSelect).getName();
        confirmYN(confirmItem);
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        confirmAll();
    }//GEN-LAST:event_btnClearActionPerformed
    public void confirmYN(String conMessage) throws HeadlessException {
        int reply = JOptionPane.showConfirmDialog(null, "Are you sure to delete"
                + " " + conMessage + " Item", "Warning!",
                JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            MenuService.delItem(modeSelect);
            refresh();
            JOptionPane.showMessageDialog(null, "Item " + conMessage
                    + " has removed");
        } else {
            JOptionPane.showMessageDialog(null, "Delete process has cancel");
        }
    }

    public void confirmAll() throws HeadlessException {
        int reply = JOptionPane.showConfirmDialog(null, "Are you sure to delete"
                + " All Item", "Warning!",
                JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            MenuService.delAll();
            refresh();
            JOptionPane.showMessageDialog(null, "All Item has removed");
        } else {
            JOptionPane.showMessageDialog(null, "Delete process has cancel");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtAmount;
    private javax.swing.JTextField edtBrand;
    private javax.swing.JTextField edtID;
    private javax.swing.JTextField edtName;
    private javax.swing.JTextField edtPrice;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lstItem;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JLabel txtBaht;
    private javax.swing.JLabel txtBrand;
    private javax.swing.JLabel txtID;
    private javax.swing.JLabel txtItem;
    private javax.swing.JLabel txtMode;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtPrice;
    private javax.swing.JLabel txtSumItem;
    private javax.swing.JLabel txtSumPrice;
    private javax.swing.JLabel txtTotalItem;
    private javax.swing.JLabel txtTotalPrice;
    // End of variables declaration//GEN-END:variables
}
