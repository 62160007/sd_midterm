/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.softdev_midterm;

import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tatar
 */
public class MenuService {

    private static ArrayList<Item> itemList = new ArrayList<>();

    static {
        load();
    }

    public static void addItem(Item item) {
        itemList.add(item);
        save();
    }

    public static void delItem(Item item) {
        itemList.remove(item);
        save();
    }

    public static void delItem(int index) {
        itemList.remove(index);
        save();
    }

    public static void delAll() {
        itemList = new ArrayList<>();
        save();
    }

    public static ArrayList<Item> getItem() {
        return itemList;
    }

    public static Item getItem(int index) {
        return itemList.get(index);
    }

    public static int getAmount(int index) {
        return itemList.get(index).getAmount();
    }

    public static double getPrice(int index) {
        return itemList.get(index).getPrice();
    }

    public static int getSize() {
        return itemList.size();
    }

    public static void updateItem(int index, Item item) {
        itemList.set(index, item);
        save();
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            saveExtrack();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MenuService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    private static void saveExtrack() throws IOException, 
            FileNotFoundException {
        File file;
        FileOutputStream fos;
        ObjectOutputStream oos;
        file = new File("tatar.bin");
        fos = new FileOutputStream(file);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(itemList);
        oos.close();
        fos.close();
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            loadExtrack();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MenuService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MenuService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    private static void loadExtrack() throws FileNotFoundException, IOException,
            ClassNotFoundException {
        File file;
        FileInputStream fis;
        ObjectInputStream ois;
        file = new File("tatar.bin");
        fis = new FileInputStream(file);
        ois = new ObjectInputStream(fis);
        itemList = (ArrayList<Item>) ois.readObject();
        ois.close();
        fis.close();
    }
}
